<?php
/**
 * "This Domain is For Sale" from HTMLPIE.COM :)
 * © HTMLPIE.COM . All rights reserved.
 *
 * @file
 * The front page, which contains the form.
 *
 * @version 3.3
 *
 */

 // This has to be at the very top of the file, before anything else.
require_once('HPDFS/includes/dfs_main.php');
?>
<!DOCTYPE html>
<!--[if lt IE 7]>     <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="<?php echo dfs_language(); ?>"><![endif]-->
<!--[if IE 7]>        <html class="no-js lt-ie9 lt-ie8" lang="<?php echo dfs_language(); ?>"><![endif]-->
<!--[if IE 8]>        <html class="no-js lt-ie9" lang="<?php echo dfs_language(); ?>"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="<?php echo dfs_language(); ?>"><!--<![endif]-->
  <head>
    <title><?php echo strtoupper(difs_domain_name(true) .' '. DFS_L_IS_FOR_SALE); ?></title><?php // Should not be removed. ?>
    <meta charset="utf-8"><?php // Should not be removed. ?>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=yes"><?php // This part is required for mobile support. ?>

    <meta property="og:site_name" content="<?php echo difs_domain_name(true); ?>">
    <meta property="og:type" content="article">
    <meta property="og:title" content="<?php echo strtoupper(difs_domain_name(true) .' '. DFS_L_IS_FOR_SALE); ?>">
    <meta property="og:url" content="<?php echo $current_url; ?>">
    <meta property="og:description" content="<?php echo str_replace(array("\n","\r"),'', trim(strip_tags(stripslashes(substr($description, 0, strrpos($description, ' ', 100)).'...')))); ?>">
    <meta name="description" content="<?php echo str_replace(array("\n","\r"),'', trim(strip_tags(stripslashes(substr($description, 0, strrpos($description, ' ', 100)).'...')))); ?>">

    <link rel="stylesheet" href="HPDFS/css/normalize.min.css" media="all" /><?php // Can be removed. ?>
    <link rel="stylesheet" href="HPDFS/css/dfs_style.css" media="all" /><?php // Should not be removed. ?>

    <script type="text/javascript" src="HPDFS/javascript/modernizr.custom-2.7.1.min.js"></script>
    <!--[if IE 8 ]>
    <script type="text/javascript" src="HPDFS/javascript/selectivizr-min.js"></script>
    <![endif]-->

    <!--[if gte IE 9]>
      <style type="text/css">body,h1,a,.result,input{filter:none}</style><?php // This is just for the gradients (on IE9) in the default style, remove it if you don't need it. ?>
    <![endif]-->
<style>
	body { background:url(/bg1.png) }
</style>
  </head>
  <body>

    <div id="main_wrapper"><?php // This can be removed. ?>

      <?php // ANYTHING AFTER THIS LINE SHOULD NOT BE REMOVED UNLESS YOU ARE SURE WHAT YOU ARE DOING. ?>
      <div id="difs_main" class="clearfix">
        <h1><a href="<?php echo $current_url; ?>"><strong><?php echo difs_domain_name(true) . '</strong> ' . DFS_L_IS_FOR_SALE; ?></a></h1>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- WildCard -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-7937169317942721"
     data-ad-slot="8341248987"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
        <?php
          $language_list = dfs_language_list('code');
          if (!empty($language_list) && DFS_C_LANGUAGE_MENU === TRUE) {
            echo $language_list;
          }
        ?>
        <?php echo $description; ?>
        <?php echo $results; ?>

        <form id="dfs_form" action="<?php echo $current_url; ?>" method="post">
          <fieldset>

            <legend><?php echo DFS_L_MAKE_AN_OFFER; ?></legend>

            <div class="row">

              <div class="column name">
                <label for="name"><?php echo DFS_L_YOUR_NAME; ?>:</label>
                <input type="text" name="name" id="name" size="35" value="<?php if ($show_data == TRUE) { echo $_POST['name']; } ?>"<?php if (DFS_C_WAI_ARIA) {?> role="textbox"<?php } ?> />
              </div>

              <div class="column email">
                <label for="email"><?php echo DFS_L_YOUR_EMAIL; ?>: <span class="asterisk">(<?php echo DFS_L_REQUIRED; ?>)</span></label>
                <input type="text" name="email" id="email"<?php if (in_array('email', $problematic)) { echo ' class="field_error"'; }?>  size="35" value="<?php if ($show_data == TRUE) { echo $_POST['email']; } ?>"<?php if (DFS_C_WAI_ARIA) {?> role="textbox" aria-required="true"<?php } ?> />
              </div>

            </div>

            <div class="row">

              <div class="column phone">
                <label for="phone"><?php echo DFS_L_YOUR_PHONE; ?>:</label>
                <input type="text" name="phone" id="phone" size="35" value="<?php if ($show_data == TRUE) { echo $_POST['phone']; } ?>"<?php if (DFS_C_WAI_ARIA) {?> role="textbox"<?php } ?> />
              </div>

              <div class="column price">
              <?php if (DFS_C_FIXED_PRICE == 0) { ?>
                <label for="offer"><?php echo DFS_L_YOUR_OFFER .' '. trim(DFS_C_CURRENCY) .')'; ?>: <span class="asterisk">(<?php echo DFS_L_REQUIRED; ?>)</span></label>
                <input type="text" name="offer" id="offer"<?php if (in_array('offer', $problematic)) { echo ' class="field_error"'; }?>  size="35" value="<?php if ($show_data == TRUE) { echo $_POST['offer']; } ?>"<?php if (DFS_C_WAI_ARIA) {?> role="textbox" aria-required="true"<?php } ?> />
                <?php if (DFS_C_MINIMUM > 0 && DFS_C_DISPLAY_MINIMUM) { ?>
                <span class="small"><?php echo DFS_L_MINIMUM_OFFER .' '. trim(DFS_C_MINIMUM) .' '. DFS_C_CURRENCY; ?></span>
                <?php } ?>
              <?php } else { ?>
                <label><?php echo DFS_L_OUR_PRICE; ?>:</label>
                <div id="price"><?php echo DFS_C_FIXED_PRICE .' '. trim(DFS_C_CURRENCY); ?></div>
              <?php } ?>
              </div>

            </div>

            <?php if (DFS_C_COMMENT) { ?>
            <div class="row comment">
              <label for="comment"><?php echo DFS_L_COMMENT; ?>:</label>
              <textarea name="comment" id="comment" rows="3" cols="5"><?php if ($show_data == TRUE) { echo $_POST['comment']; } ?></textarea>
            </div>
            <?php } ?>

            <?php if (trim(DFS_C_CAPTCHA_TYPE) != 'hiddencaptcha') { ?>
            <div class="row captcha">

              <div class="column">
                <label for="captcha"><?php echo DFS_L_CAPTCHA; ?>: <span class="asterisk">(<?php echo DFS_L_REQUIRED; ?>)</span></label>
                <img src="HPDFS/includes/dfs_captcha.php" id="theimage" width="150" height="50" alt="" />
                <input type="text" name="captcha" id="captcha"<?php if (in_array('captcha', $problematic)) { echo ' class="field_error"'; }?> size="5"<?php if (DFS_C_WAI_ARIA) {?> role="textbox" aria-required="true"<?php } ?> />
              </div>

              <div class="column">
                <p>
                  <?php echo DFS_L_ANSWER_CAPTCHA; ?><br>
                  <script type="text/javascript">
                    document.write("<a href=\"#\" onclick=\"document.getElementById('theimage').src = 'HPDFS/includes/dfs_captcha.php?' + Math.random(); return false\"><?php echo DFS_L_RELOAD_IMAGE; ?></a>");
                  </script>
                </p>
              </div>

            </div>
            <?php } else { ?>
            <div style="left:-99990px;position:absolute;top:-99999px;"><?php // Caution! Do NOT touch this div. ?>
              <label for="<?php echo $hc; ?>">Subject *:</label>
              <input  type="text" name="<?php echo $hc; ?>" id="<?php echo $hc; ?>" />
              <input type="hidden" name="<?php echo difs_token('name'); ?>" value="<?php echo difs_token('value'); ?>" />
            </div>
            <?php } ?>

            <div class="row submit">

              <input type="submit" name="submit" class="button" value="<?php echo DFS_L_SUBMIT; ?>" />
              <sub>(* <?php echo DFS_L_REQUIRED_FIELDS; ?>.)</sub>

            </div>

          </fieldset>
        </form>
      </div>

      <?php if (DFS_C_SHARE_BUTTONS) { ?>
      <div id="difs_share" class="clearfix">
        <h2><?php echo DFS_L_SHARE; ?></h2>
        <ul class="clearfix">
          <li class="twitter"><a href="https://twitter.com/intent/tweet?original_referer=<?php echo rawurlencode($current_url); ?>&amp;source=tweetbutton&amp;text=<?php echo rawurlencode($domain_name .' '. DFS_L_IS_FOR_SALE); ?>&amp;url=<?php echo rawurlencode($current_url); ?>"><?php echo DFS_L_TWITTER; ?></a></li>
          <li class="facebook"><a href="https://facebook.com/sharer.php?u=<?php echo rawurlencode($current_url); ?>"><?php echo DFS_L_FACEBOOK; ?></a></li>
          <li class="linkedin"><a href="https://linkedin.com/cws/share?url=<?php echo rawurlencode($current_url); ?>&amp;original_referer=<?php echo rawurlencode($current_url); ?>"><?php echo DFS_L_LINKEDIN; ?></a></li>
          <li class="google_plus"><a href="https://plus.google.com/share?url=<?php echo rawurlencode($current_url); ?>"><?php echo DFS_L_GOOGLE_PLUS; ?></a></li>
        </ul>
      </div>
      <?php } ?>

    </div><?php // This can be removed. ?>

    <script type="text/javascript" src="HPDFS/javascript/jquery-1.11.min.js"></script>
    <script type="text/javascript" src="HPDFS/javascript/common.js.php<?php echo (isset($_GET['l'])) ? '?l='. $_GET['l'] : ''; ?>"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-19546749-11', 'auto');
  ga('send', 'pageview');

</script>
  </body>
</html>
