<?php
/**
 * "This Domain is For Sale" from HTMLPIE.COM :)
 * � HTMLPIE.COM . All rights reserved.
 *
 * @file
 * Extra protection for the CSV folder.
 *
 * @version 3.3
 *
 */

// Just in case there was no index.html
header('Location:../../index.php');