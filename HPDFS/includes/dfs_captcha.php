<?php
/**
 * @file
 * Mathematical CAPTCHA.
 * © HTMLPIE.COM . All rights reserved.
 *
 * @version 2.2
 *
 */

  session_start();
  $number = array(rand(0,20), rand(0,20));
  $math = $number[0] . '+' . $number[1] . '=';
  $_SESSION['dfs_captcha'] = md5($number[0] + $number[1]);
  $text = imagecreate(300, 100);
  $background = imagecolorallocate($text, rand(200,220), rand(200,220), rand(200,220));
  $text_color = imagecolorallocate($text, rand(0,80), rand(0,80), rand(0,80));
  imagestring($text, 8, rand(10,90), rand(5,22), $math, $text_color);
  $text = imagerotate($text, rand(-5,5), 0);
  $hash = imagecreate(150, 50);
  $background = imagecolorallocate($hash, rand(150,220), rand(150,220), rand(150,220));
  imagecopy($hash, $text, 0, 0, rand(0,5), rand(0,10), 150, 100);
  imagedestroy($text);
  for ($i = 0; $i <= 5; $i++) {
    imageline($hash, $i*20+rand(0,25), rand(0,50), $i*20-rand(0,25), rand(0,50), imagecolorallocate($hash, rand(0,100), rand(0,100), rand(0,100)));
  }
  for ($i = 0; $i <= 5; $i++) {
    imagestring($hash, rand(1,8), rand(-10,10), $i*10-rand(0,5), '. . . . . . . . . . . .', imagecolorallocate($hash, rand(0,100), rand(0,100), rand(0,100)));
  }
  $image = imagecreatetruecolor(300, 100);
  imagecopyresampled($image, $hash, 0, 0, 0, 0, 300, 100, 150, 50);
  header("Expires: Wed, 01 Jan 1995 01:00:00 GMT");
  header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
  header("Cache-Control: no-store, no-cache, must-revalidate");
  header("Cache-Control: post-check=0, pre-check=0", false);
  header("Pragma: no-cache");
  header('Content-type: image/png');
  imagepng($image);
  imagedestroy($image);