<?php
/**
 * "This Domain is For Sale" from HTMLPIE.COM :)
 * © HTMLPIE.COM . All rights reserved.
 *
 * @file
 * The configuration file validation.
 *
 * @version 3.3
 *
 */

  if (!defined('DFS_C_SITE_OWNER_EMAIL') ||
      !defined('DFS_C_FORM_ACTION') ||
      !defined('DFS_C_MAILER') ||
      !defined('DFS_C_CSV_FILE_NAME') ||
      !defined('DFS_C_FIXED_PRICE') ||
      !defined('DFS_C_CURRENCY') ||
      !defined('DFS_C_MINIMUM') ||
      !defined('DFS_C_DISPLAY_MINIMUM') ||
      !defined('DFS_C_COMMENT') ||
      !defined('DFS_C_CAPTCHA_TYPE') ||
      !defined('DFS_C_IP_ADDRESS') ||
      !defined('DFS_C_LANGUAGE_DEFAULT') ||
      !defined('DFS_C_LANGUAGE_DETECT') ||
      !defined('DFS_C_LANGUAGE_MENU') ||
      !defined('DFS_C_SHARE_BUTTONS') ||
      !defined('DFS_C_WAI_ARIA') ||
      !defined('DFS_C_AJAX')) {
    echo 'Caution: Incomplete configuration file.';
    die();
  }

  if (trim(DFS_C_FORM_ACTION) == 'email' && (trim(DFS_C_SITE_OWNER_EMAIL) == 'mail@example.com' || trim(DFS_C_SITE_OWNER_EMAIL) == '')) {
    echo 'Caution: You must change the DFS_C_SITE_OWNER_EMAIL in the configuration file.';
    die();
  }

  if (trim(DFS_C_FORM_ACTION) == 'email' && !filter_var(trim(DFS_C_SITE_OWNER_EMAIL), FILTER_VALIDATE_EMAIL)) {
    echo 'Caution: Incorrect DFS_C_SITE_OWNER_EMAIL in the configuration file.';
    die();
  }

  if (trim(DFS_C_MAILER) == '' || (DFS_C_MAILER != 'mail' && DFS_C_MAILER != 'swiftmailer' && DFS_C_MAILER != 'swiftmailer_smtp')) {
    echo 'Caution: You must change the DFS_C_MAILER in the configuration file.';
    die();
  }

  if (DFS_C_MAILER == 'swiftmailer_smtp') {
    if (!defined('DFS_C_SMTP_SERVER') || trim(DFS_C_SMTP_SERVER) == '' || DFS_C_SMTP_SERVER == 'smtp.example.com') {
      echo 'Caution: You must add your SMTP settings in the configuration file.';
      die();
    }
  }

  if (trim(DFS_C_FORM_ACTION) == 'csv' && (DFS_C_CSV_FILE_NAME == '1234567' || trim(DFS_C_CSV_FILE_NAME) == '')) {
    echo 'Caution: You must change the DFS_C_CSV_FILE_NAME in the configuration file.';
    die();
  }

  if (!is_numeric(DFS_C_FIXED_PRICE) || (DFS_C_FIXED_PRICE < 0 || is_bool(DFS_C_FIXED_PRICE))) {
    echo 'Caution: You must change the DFS_C_FIXED_PRICE in the configuration file.';
    die();
  }

  if (trim(DFS_C_CURRENCY) == '') {
    echo 'Caution: You must change the DFS_C_CURRENCY in the configuration file.';
    die();
  }

  if (!is_numeric(DFS_C_MINIMUM) || DFS_C_MINIMUM < 0) {
    echo 'Caution: You must change the DFS_C_MINIMUM in the configuration file.';
    die();
  }

  if (!is_bool(DFS_C_DISPLAY_MINIMUM)) {
    echo 'Caution: You must change the DFS_C_DISPLAY_MINIMUM in the configuration file.';
    die();
  }

  if (!is_bool(DFS_C_COMMENT)) {
    echo 'Caution: You must change the DFS_C_COMMENT in the configuration file.';
    die();
  }

  if (trim(DFS_C_CAPTCHA_TYPE) == '' || (DFS_C_CAPTCHA_TYPE != 'mathcaptcha' && DFS_C_CAPTCHA_TYPE != 'hiddencaptcha')) {
    echo 'Caution: You must change the DFS_C_CAPTCHA_TYPE in the configuration file.';
    die();
  }

  if (!is_bool(DFS_C_IP_ADDRESS)) {
    echo 'Caution: You must change the DFS_C_IP_ADDRESS in the configuration file.';
    die();
  }

  if (trim(DFS_C_LANGUAGE_DEFAULT) != '' && !file_exists(dirname(__FILE__) .'/../language/dfs_language.'. DFS_C_LANGUAGE_DEFAULT .'.php')) {
    echo 'Caution: You must change the DFS_C_LANGUAGE_DEFAULT in the configuration file.';
    die();
  }

  if (!is_bool(DFS_C_LANGUAGE_MENU)) {
    echo 'Caution: You must change the DFS_C_LANGUAGE_MENU in the configuration file.';
    die();
  }

  if (!is_bool(DFS_C_SHARE_BUTTONS)) {
    echo 'Caution: You must change the DFS_C_SHARE_BUTTONS in the configuration file.';
    die();
  }

  if (!is_bool(DFS_C_WAI_ARIA)) {
    echo 'Caution: You must change the DFS_C_WAI_ARIA in the configuration file.';
    die();
  }

  if (!is_bool(DFS_C_AJAX)) {
   echo 'Caution: You must change the DFS_C_AJAX in the configuration file.';
   die();
  }
