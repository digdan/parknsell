<?php
/**
 * "This Domain is For Sale" from HTMLPIE.COM :)
 * © HTMLPIE.COM . All rights reserved.
 *
 * @file
 * The main file.
 *
 * @version 3.3
 *
 */

  // Starting a new session.
  if (version_compare(PHP_VERSION, '5.4.0') >= 0) {
    if (session_status() == PHP_SESSION_NONE) {
      session_start();
    }
  } elseif(session_id() === '') {
    session_start();
  }

  function difs_domain_name($convert_idna = false) {
    $domain_name = preg_replace('/^www\./','', ((isset($_SERVER['HTTP_HOST'])) ? $_SERVER['HTTP_HOST'] : $_SERVER['SERVER_NAME']));
    if ($convert_idna && substr($domain_name, 0, 4) == 'xn--') {
      if (function_exists('idn_to_utf8')) {
        $domain_name = idn_to_utf8($domain_name);
      } else {
        require_once('idna/idna_convert.class.php');
        $IDN = new idna_convert();
        $domain_name = $IDN->decode($domain_name);
        $domain_name = $domain_name;
      }
    }
    return $domain_name;
  }
  // Crystal clear :)
  $current_url   = 'http://'. difs_domain_name() . $_SERVER['REQUEST_URI'];
  // Path to the current directory.
  $current_directory = dirname(__FILE__);
  // Generating a name for the hidden CAPTCHA field.
  $hc = md5(difs_domain_name() . __FILE__ . date('m'));
  // Form messages.
  $results = '';
  // This is for knowing when the form is not submitted
  // hence the need to remember the previously entered data.
  $show_data = FALSE;
  // This is for highlighting problematic fields.
  $problematic = array();

  // Crystal clear :)
  require_once('dfs_configuration.php');
  require_once('dfs_configuration.validate.php');
  require_once('dfs_language.php');

  function difs_token($output = 'name') {
    if ($output == 'name') {
      if (isset($_SESSION['hpdfs_token_name'])) {
        return $_SESSION['hpdfs_token_name'];
      } else {
        $token_name = substr(str_shuffle("012340123401234abcdfhlmnpqrtuvyABCDFHLMNPQTUVY"), 0, 10);
        $_SESSION['hpdfs_token_name'] = $token_name;
        return $token_name;
      }
    } else {
      if (isset($_SESSION['hpdfs_token_value'])) {
        return $_SESSION['hpdfs_token_value'];
      } else {
        $token = substr(str_shuffle("012340123401234abcdfhlmnpqrtuvyABCDFHLMNPQTUVY"), 0, 30);
        $_SESSION['hpdfs_token_value'] = $token;
        return $token;
      }
    }
  }

  // Including the description file.
  $description = '';
  if (file_exists($current_directory .'/../description/'. dfs_language() .'.description.'. strtolower(difs_domain_name()) .'.txt')) {
    $description = $current_directory .'/../description/'. dfs_language() .'.description.'. strtolower(difs_domain_name()) .'.txt';
  }
  elseif (file_exists($current_directory .'/../description/description.'. strtolower(difs_domain_name()) .'.txt')) {
    $description = $current_directory .'/../description/description.'. strtolower(difs_domain_name()) .'.txt';
  }
  elseif (file_exists($current_directory .'/../description/'. dfs_language(). '.description.txt')) {
    $description = $current_directory .'/../description/'. dfs_language(). '.description.txt';
  }
  elseif (file_exists($current_directory .'/../description/description.txt')) {
    $description = $current_directory .'/../description/description.txt';
  }
  $description = (!empty($description)) ? file_get_contents($description) : '';

  // Once form submitted.
  if (isset($_POST['email'])) {

    // An empty array to store form messages.
    $result = array();

    // Data sanitization.
    $name = str_replace(array('"',','), '', trim(strip_tags(stripslashes($_POST['name']))));
    $email = filter_var(strip_tags(trim(stripslashes($_POST['email']))), FILTER_SANITIZE_EMAIL);
    $phone = str_replace(array('"',','), '', trim(strip_tags(stripslashes($_POST['phone']))));
    if (DFS_C_FIXED_PRICE == 0) {
      $offer = str_replace(array('"',','), '', trim(strip_tags(stripslashes($_POST['offer']))));
    } else {
      $offer = DFS_C_FIXED_PRICE;
    }
    $comment = (DFS_C_COMMENT) ? trim(strip_tags(stripslashes($_POST['comment']))) : '';

    // Checking the e-mail address, first checking whether one is provided.
    if (empty($email)) {
      $result[] = DFS_L_NO_EMAIL;
      $problematic[] = 'email';
      $show_data = TRUE;
    }
    // Then checking whether it is valid!
    else {
      list($user, $host) = explode("@", $email);
      if (!filter_var($email, FILTER_VALIDATE_EMAIL) || (function_exists('checkdnsrr') && !checkdnsrr($host, "MX") && !checkdnsrr($host, "A"))) {
        $result[] = DFS_L_EMAIL_ADDRESS_IS_INCORRECT;
        $problematic[] = 'email';
        $show_data = TRUE;
      }
    }

    // Checking the offer, first we'll check whether there is an offer at all.
    if (DFS_C_FIXED_PRICE == 0) {
      if (empty($offer) || !is_numeric($offer)) {
        $result[] = DFS_L_NO_OFFER;
        $problematic[] = 'offer';
        $show_data = TRUE;
      }
      // Then we check the amount should a minimum is set.
      elseif (DFS_C_MINIMUM > 0 && $offer < DFS_C_MINIMUM) {
        $result[] = DFS_L_DOES_NOT_MEET_MINIMUM;
        $problematic[] = 'offer';
        $show_data = TRUE;
      }
    }

    // If mathematical is being used.
    if (trim(DFS_C_CAPTCHA_TYPE) == 'mathcaptcha') {
      // Checking the CAPTCHA, first we check whether it's answered.
      if (empty($_POST['captcha'])) {
        $result[] = DFS_L_PLEASE_ENTER_CAPTCHA;
        $problematic[] = 'captcha';
        $show_data = TRUE;
      }
      // Then we'll check whether the answer is correct.
      elseif (md5($_POST['captcha']) != $_SESSION['dfs_captcha']) {
        $result[] = DFS_L_CAPTCHA_IS_INCORRECT;
        $problematic[] = 'captcha';
        $show_data = TRUE;
      }
    }
    // Otherwise, we'll check if the hidden field of the hidden CAPTCHA was filled.
    elseif (isset($_POST[$hc]) && !empty($_POST[$hc])) {
      $result[] = DFS_L_SOMETHING_WRONG;
      $show_data = TRUE;
    }

    // Checking the token.
    if (!isset($_POST[difs_token()]) || $_POST[difs_token()] != difs_token('value')) {
      $result[] = DFS_L_SOMETHING_WRONG;
      $show_data = TRUE;
    }

    // Flood protection.
    if (isset($_SESSION['hpdfs_antiflood'])) {
      // User has to wait for 2 minutes before sending another email,
      // and insisting on sending email will increase the delay.
      $latest_message = $_SESSION['hpdfs_antiflood'][0];
      $num_fail = $_SESSION['hpdfs_antiflood'][1];
      $num_success = $_SESSION['hpdfs_antiflood'][2];
      $time_limit = ($num_fail > 5) ? (30 * 60) : (2 * 60);
      if ($num_success > 10) {
        $time_limit = 120 * 60;
      }
      $time_passed = time() - $latest_message;
      if ($time_passed < $time_limit) {
        $_SESSION['hpdfs_antiflood'][1]++;
        switch ($time_limit) {
          case (2 * 60):
          $result[] = DFS_L_WAIT_TWO_MINUTES;
          break;
          case (30 * 60):
          $result[] = DFS_L_WAIT_THIRTY_MINUTES;
          break;
          case (120 * 60):
          $result[] = DFS_L_WAIT_TWO_HOURS;
          break;
        }
        $show_data = TRUE;
      }
    }

    // Everything fine?
    if (empty($result)) {
      // Choosing between emailing or saving as CSV.
      $success = false;
      if (trim(DFS_C_FORM_ACTION) == 'email') {
        // Starting to prepare the e-mail as everything seems to be just fine.
        $body =  DFS_L_NAME .": $name <br><br>\n";
        $body .= DFS_L_EMAIL .": $email <br><br>\n";
        $body .= DFS_L_PHONE .": $phone <br><br>\n";
        $body .= ((DFS_C_FIXED_PRICE == 0) ? DFS_L_OFFER : DFS_L_PRICE) .": $offer ". trim(DFS_C_CURRENCY) ."<br><br>";
        if (DFS_C_COMMENT) {
          $body .= DFS_L_COMMENT .': '. $comment ."<br><br>";
        }
        if (DFS_C_IP_ADDRESS) {
          if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARTDED_FOR'] != '') {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
          } else {
            $ip = $_SERVER['REMOTE_ADDR'];
          }
          $body .= DFS_L_IP .': '. $ip;
        }

        // SwiftMailer library.
        if (strpos(trim(DFS_C_MAILER), 'swiftmailer') !== FALSE) {
          require_once($current_directory . '/swiftmailer/swift_required.php');

          // If language is Japanese.
          if (dfs_language() == 'ja') {
            function sm_japanese() {
              Swift_DependencyContainer::getInstance()
                ->register('mime.qpheaderencoder')
                ->asAliasOf('mime.base64headerencoder');
              Swift_Preferences::getInstance()->setCharset('iso-2022-jp');
            }
            Swift::init('sm_japanese');
          }

          // Send the e-mail.
          if (trim(DFS_C_MAILER) == 'swiftmailer') {
            $transport = Swift_MailTransport::newInstance();
          }
          if (trim(DFS_C_MAILER) == 'swiftmailer_smtp') {
            $transport = Swift_SmtpTransport::newInstance(trim(DFS_C_SMTP_SERVER), DFS_C_SMTP_PORT)
              ->setUsername(trim(DFS_C_SMTP_USERNAME))
              ->setPassword(trim(DFS_C_SMTP_PASSWORD));
            if (DFS_C_SMTP_SSL) {
              $transport->setEncryption('ssl');
            }
          }

          try {
            $mailer = Swift_Mailer::newInstance($transport);
            $new_offer = Swift_Message::newInstance()
              ->setContentType('text/html')
              ->setSubject(DFS_L_NEW_EMAIL .' '. difs_domain_name(true))
              ->setFrom(array($email => $name))
              ->setTo(array(trim(DFS_C_SITE_OWNER_EMAIL)))
              ->setReplyTo(array($email => $name))
              ->setReturnPath($email)
              ->setBody($body);
            $success = $mailer->send($new_offer);
          } catch(Swift_TransportException $e) {
            // Storing the error message if failed.
            $result[] = $e->getMessage();
          }
        }
        // Or plain mail() function.
        else {
          // These are sometimes required, depending on your web server.
          ini_set('sendmail_from', $email);
          date_default_timezone_set('UTC');
          $header = "From: ". ((empty($name)) ? $email : $name .' <'. $email .'>') ."\r\n";
          $header .= "Reply-To: $email\r\n";
          $header .= "Return-Path: $email\r\n";
          $header .= "MIME-Version: 1.0\r\n";
          $header .= "Content-Type: text/html; charset=UTF-8\r\n";
          $header .= "Content-Transfer-Encoding: quoted-printable\r\n";
          $success = mail(trim(DFS_C_SITE_OWNER_EMAIL), DFS_L_NEW_EMAIL .' '. difs_domain_name(true), $body, $header);
        }
      }
      // Saving as CSV file.
      else {
        $csv = '"'. $offer .'","'. $name .'","'. $email .'","'. $phone .'","'. ((DFS_C_IP_ADDRESS) ? $ip : '') .'","'. ((DFS_C_COMMENT) ? str_replace(array('"',','), '',$comment) : '') .'","'. difs_domain_name(true) .'","'. date('Y-m-d') ."\"\n";
        $filename = $current_directory .'/../csv/'. trim(DFS_C_CSV_FILE_NAME) . '.csv';
        $handle = fopen($filename, 'a') or die('Cannot open file: '. $filename);
        // Another way is to use fputcsv() but this way (using fwrite) is a bit more flexible.
        $success = fwrite($handle, $csv);
        fclose($handle);
      }

      if ($success) {
        // Creating a session for anti-flood protection.
        // If one already exists this resets it.
        $num_sent = (isset($_SESSION['hpdfs_antiflood'])) ? (int)$_SESSION['hpdfs_antiflood'][2] + 1 : 1;
        $_SESSION['hpdfs_antiflood'] = array(time(), 1, $num_sent);
        // Crystal clear :)
        $result[] = DFS_L_THANKS;
        $show_data = FALSE;
      } else {
        $result[] = DFS_L_SOMETHING_WRONG;
        $show_data = TRUE;
      }
    }

    if (count($result) > 0) {
      $results = '<div class="result clearfix'. (($show_data === TRUE) ? ' error' : '') .'">';
      $results .= '<ul>';
      foreach($result as $m) {
        $results .= '<li>'. $m .'</li>';
      }
      $results .= '</ul>';
      $results .= '</div>';
    }

    // JSON output for AJAX.
    if (isset($_POST['ajax'])) {
      echo json_encode(array(
        'status' => ($show_data) ? FALSE : TRUE,
        'result' => $results,
      ));
    }
  }
