<?php
/**
 * "This Domain is For Sale" from HTMLPIE.COM :)
 * � HTMLPIE.COM . All rights reserved.
 *
 * @file
 * Configuration file.
 *
 * @version 3.3
 *
 */

  /**
   * Your e-mail address.
   * Example: mail@example.com
   */
  define('DFS_C_SITE_OWNER_EMAIL',    'dan@interwebdev.com');

  /**
   * By default the script emails the bids to you,
   * you can change this so it stores the bids in a CSV file on the web server.
   * ALSO SET A CSV FILE NAME BELOW IF YOU ARE GOING TO USE CSV.
   * Options: email & csv
   * Default: email
   * Recommended: email
   */
  define('DFS_C_FORM_ACTION',         'email');

  /**
   * Choose a mailer function.
   * Options: mail & swiftmailer & swiftmailer_smtp
   * Default: swiftmailer
   */
  define('DFS_C_MAILER',              'swiftmailer_smtp');

  /**
   * SMTP configuration.
   */
  define('DFS_C_SMTP_SERVER',         'smtp.sendgrid.net');
  define('DFS_C_SMTP_PORT',           587);
  define('DFS_C_SMTP_USERNAME',       'digdan');
  define('DFS_C_SMTP_PASSWORD',       'e6wSeHbbcj4k3b9M');
  define('DFS_C_SMTP_SSL',            FALSE);

  /**
   * YOU MUST CHANGE THIS!
   * If you are going to use the CSV (explained above) option you MUST choose a
   * name for the CSV file, otherwise your visitors might be able to DOWNLOAD THE CSV FILE!
   * Example: a2Bo7QlRrZ0q1fc5
   * Please note this is supposed to be a file name hence only letters & numbers should be used.
   */
  define('DFS_C_CSV_FILE_NAME',       '1234567');

  /**
   * Sell for a fixed price.
   * It removes the offer field and also makes the minimum price option ineffective.
   * Example: 100
   * Default: 0
   */
  define('DFS_C_FIXED_PRICE',         0); // 0 means letting visitors make offers.

  /**
   * The default currency.
   * Default: USD.
   */
  define('DFS_C_CURRENCY',            'USD');

  /**
   * You can set a minimum acceptable price.
   * Example: 10
   * Default: 0 (which means no limit).
   */
  define('DFS_C_MINIMUM',             50);

  /**
   * If enabled, visitors will see the minimum amount in the form.
   * Options: TRUE FALSE
   * Default: FALSE
   */
  define('DFS_C_DISPLAY_MINIMUM',     TRUE);

  /**
   * Allows bidders to leave a comment.
   * Options: TRUE FALSE
   * Default: TRUE
   */
  define('DFS_C_COMMENT',             TRUE);

  /**
   * CAPTCHA type
   * There are two ways available to avoid spam: mathcaptcha & hiddencaptcha
   * Default: mathcaptcha
   */
  define('DFS_C_CAPTCHA_TYPE',        'hiddencaptcha');

  /**
   * Log bidders' IP address
   * Options: TRUE FALSE
   * Default: FALSE
   */
  define('DFS_C_IP_ADDRESS',          TRUE);

  /**
   * Default language
   * Leave empty or use a language code.
   * Example: fr
   * Note, a file named language.THELANGUAGECODE.php must exist.
   * A list of languages codes is available at http://w3schools.com/tags/ref_language_codes.asp
   * Default: empty (if empty and below option enabled, visitors' Web browser
   * language will be used (only if a translation is available obviously).
   * Default: empty
   */
  define('DFS_C_LANGUAGE_DEFAULT',    '');

  /**
   * Detect visitor's Web browser language.
   * Options: TRUE FALSE
   * Default: TRUE
   */
  define('DFS_C_LANGUAGE_DETECT',     TRUE);

  /**
   * Let visitors switch languages.
   * Options: TRUE FALSE
   * Default: TRUE
   */
  define('DFS_C_LANGUAGE_MENU',       TRUE);

  /**
   * Include share buttons.
   * Options: TRUE FALSE
   * Default: FALSE
   */
  define('DFS_C_SHARE_BUTTONS',       FALSE);

  /**
   * Adds WAI-ARIA roles to the form fields.
   * Please note that not every DTD is suitable for WAI-ARIA, the best being probably <!doctype html>
   * more information: http://www.w3.org/TR/wai-aria/appendices#xhtml_dtd
   * Options: TRUE FALSE
   * Default: FALSE
   */
  define('DFS_C_WAI_ARIA',            FALSE);

  /**
   * Use AJAX to submit the form.
   * Options: TRUE FALSE
   * Default: TRUE
   */
  define('DFS_C_AJAX',                TRUE);
