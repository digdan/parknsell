<?php
/**
 * "This Domain is For Sale" from HTMLPIE.COM :)
 * © HTMLPIE.COM . All rights reserved.
 *
 * @file
 * Form validation and AJAX submission.
 *
 * @version 3.3
 *
 */

  // Starting a new session.
  if (version_compare(PHP_VERSION, '5.4.0') >= 0) {
    if (session_status() == PHP_SESSION_NONE) {
      session_start();
    }
  } elseif(session_id() === '') {
    session_start();
  }

  // Loading prerequisites.
  require_once('../includes/dfs_configuration.php');
  require_once('../includes/dfs_configuration.validate.php');
  require_once('../includes/dfs_language.php');
  header('Content-type: application/javascript');
?>
;(function() {

  function isValidEmailAddress(email) {
    var emailReg = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i),
    valid = emailReg.test(email);
    return ((valid) ? true : false);
  }

  jQuery(document).ready(function() {
    $('#dfs_form').submit(function(e) {
      var $this = $(this),
      useAJAX = <?php echo ((DFS_C_AJAX) ? 'true' : 'false'); ?>,
      captchaType = "<?php echo DFS_C_CAPTCHA_TYPE; ?>",
      fixedPrice = <?php echo (DFS_C_FIXED_PRICE != 0) ? 'true' : 'false'; ?>,
      error = new Array(),
      requiredMissing = false,
      $email = $('#email'),
      emailValue = $.trim($email.val());
      if (!fixedPrice) {
        var $offer = $('#offer'),
        offerValue = $.trim($offer.val());
      }

      if (emailValue == '') {
        requiredMissing = true;
        $email.addClass('field_error');
      }
      if (emailValue != '' && !isValidEmailAddress(emailValue)) {
        error.push("- <?php echo DFS_L_EMAIL_ADDRESS_IS_INCORRECT; ?>.");
        if (!$email.hasClass('field_error')) {
          $email.addClass('field_error');
        }
      }
      if (!fixedPrice && offerValue == '') {
        requiredMissing = true;
        $offer.addClass('field_error');
      }
      if (captchaType == 'mathcaptcha' && $.trim($('#captcha').val()) == '') {
        requiredMissing = true;
        $('#captcha').addClass('field_error');
      }
      if (requiredMissing) {
        error.push("- <?php echo DFS_L_ENTER_REQUIRED_FIELDS; ?>.");
      }

      if (error.length > 0) {
        e.preventDefault();
        alert(error.join("\r\n"));
      }
      else {
        $('.field_error', $this).removeClass('field_error');

        if (useAJAX) {
          $this.prev('.result').remove();
          var $load = $('<div class="result please-wait" style="display:none"><div class="loading-wrapper"><div class="loading load01"></div><div class="loading load02"></div><div class="loading load03"></div><div class="loading load04"></div><div class="loading load05"></div><div class="loading load06"></div><div class="loading load07"></div><div class="loading load08"></div></div><ul><li>' + "<?php echo DFS_L_PLEASE_WAIT; ?>" + '</li></ul></div>');
          $this.before($load).prev($load).fadeIn('slow');
          $.post('HPDFS/includes/dfs_main.php', $this.serialize() + '&ajax=1', function(d) {
              if (captchaType == 'mathcaptcha') {
                jQuery('#theimage').attr('src', 'HPDFS/includes/dfs_captcha.php?' + Math.random());
                jQuery('#captcha').val('');
              }
              var data = $.parseJSON($.trim(d));
              $this.parent('div').removeClass('success error').addClass((data.status) ? 'success' : 'error');
              $load.fadeOut('slow', function() {
                var $result = $(data.result).hide();
                $load.fadeOut('slow', function() {
                  $this.before($result).prev($result).fadeIn('slow', function() {
                    if (data.status) {
                      $('input[type="text"], textarea', $this).val('');
                    }
                  });
                });
              });
            }
          );
          e.preventDefault();
        }
      }
    });
  });
})(jQuery);