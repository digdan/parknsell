<?php
/**
 * "This Domain is For Sale" from HTMLPIE.COM :)
 * © HTMLPIE.COM . All rights reserved.
 *
 * @file
 * The default language file.
 *
 * @name English
 *
 */

  define('DFS_L_IS_FOR_SALE',                "is for sale!");
  define('DFS_L_MAKE_AN_OFFER',              "Make an offer!");
  define('DFS_L_YOUR_NAME',                  "Your name");
  define('DFS_L_YOUR_EMAIL',                 "E-mail address");
  define('DFS_L_YOUR_PHONE',                 "Phone number");
  define('DFS_L_YOUR_OFFER',                 "Your bid (in");
  define('DFS_L_OUR_PRICE',                  "Our Price");
  define('DFS_L_COMMENT',                    "Comment");
  define('DFS_L_MINIMUM_OFFER',              "Minimum bid:");
  define('DFS_L_CAPTCHA',                    "CAPTCHA");
  define('DFS_L_ANSWER_CAPTCHA',             "Solve this simple equation to prove you are a human.");
  define('DFS_L_RELOAD_IMAGE',               "Reload image");
  define('DFS_L_SUBMIT',                     "Submit");

  define('DFS_L_PLEASE_WAIT',                "Please wait...");
  define('DFS_L_REQUIRED',                   "Required");
  define('DFS_L_REQUIRED_FIELDS',            "Required fields");
  define('DFS_L_LANGUAGE_MENU',              "Choose your language:");

  define('DFS_L_SHARE',                      "Or tell your friends!");
  define('DFS_L_TWITTER',                    "Twitter");
  define('DFS_L_FACEBOOK',                   "Facebook");
  define('DFS_L_LINKEDIN',                   "LinkedIn");
  define('DFS_L_GOOGLE_PLUS',                "Google +");

  define('DFS_L_PLEASE_ENTER_CAPTCHA',       "Please solve the equation.");
  define('DFS_L_CAPTCHA_IS_INCORRECT',       "Equation not correct, please try again.");
  define('DFS_L_EMAIL_ADDRESS_IS_INCORRECT', "E-mail address is not correct.");
  define('DFS_L_NO_EMAIL',                   "Enter an e-mail address please.");
  define("DFS_L_ENTER_REQUIRED_FIELDS",      "Please fill all the required fields.");
  define('DFS_L_NO_OFFER',                   "No offer provided.");
  define('DFS_L_DOES_NOT_MEET_MINIMUM',      "Your offer does not meet the minimum bid price.");
  define('DFS_L_THANKS',                     "Thank you very much! we'll review your offer as soon as possible.");
  define('DFS_L_SOMETHING_WRONG',            "Something went wrong, please try again.");
  define("DFS_L_WAIT_TWO_MINUTES",           "Please wait for two minutes before submitting the form again.");
  define("DFS_L_WAIT_THIRTY_MINUTES",        "Please wait for 30 minutes before submitting the form again.");
  define("DFS_L_WAIT_TWO_HOURS",             "Please wait for 2 hours before submitting the form again.");

  define('DFS_L_NEW_EMAIL',                  "New offer for");
  define('DFS_L_NAME',                       "Name");
  define('DFS_L_EMAIL',                      "E-mail");
  define('DFS_L_PHONE',                      "Phone");
  define('DFS_L_OFFER',                      "Offer");
  define('DFS_L_PRICE',                      "Price");
  define('DFS_L_IP',                         "IP address");